const COLORS = {
    BG_MAIN: "#3498db",
    BG_SUB: "#2980b9",
    LIGHTGREY: "#bdc3c7",
    DARKGREY: "#7f8c8d",
    BLACK: "#34495e",
    GREEN: "#27ae60",
    RED: "#c0392b",
    WHITE: "#ffffff"
}

export default COLORS