import React, { Component } from 'react'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'
import COLORS from '../../../colors'

class ActionButton extends Component {
    render = () => (
        <TouchableOpacity
            onPress={this.props.onPress}>
            <View style={[styles.container, { backgroundColor: this.props.color }, this.props.style]}>
                <Text style={styles.text}>{this.props.title.toUpperCase()}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        height: 90,
        width: 90,
        borderRadius: 90,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    text: {
        color: COLORS.WHITE,
        fontSize: 55,
        fontWeight: "700"
    }
})


export default ActionButton
