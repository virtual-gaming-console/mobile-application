import React, { Component } from 'react'
import { TouchableOpacity, View, Image, StyleSheet } from 'react-native'
import icon from '../../../assets/icon.png'

class MenuButton extends Component {
  render = () => (
    <TouchableOpacity
      onPress={this.props.onPress}>
      <View style={[styles.container, this.props.style]}>
        <Image
          style={{ width: 50, height: 50 }}
          source={icon}
        />
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    width: 90,
    backgroundColor: "#202739",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  }
})


export default MenuButton
