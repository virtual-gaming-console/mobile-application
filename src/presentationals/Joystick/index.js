import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import COLORS from '../../../colors'

class Joystick extends Component {
    render = () => (
        <View style={styles.container}>
            <View style={styles.firstCircle}></View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: "relative",
        alignItems: 'center',
        justifyContent: 'center'
    },
    firstCircle: {
        backgroundColor: COLORS.BLACK,
        width: 50,
        height: 50,
        borderRadius: 50
    }
})

export default Joystick