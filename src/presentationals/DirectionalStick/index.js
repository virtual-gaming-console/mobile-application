import React, { Component } from 'react'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'
import COLORS from '../../../colors'

class DirectionalStick extends Component {
    render = () => (
        <View style={styles.container}>
            <View style={[styles.square, { backgroundColor: 'transparent' }]}></View>
            <SquareButton onPress={this.props.onUpButtonPress} />
            <View style={[styles.square, { backgroundColor: 'transparent' }]}></View>
            <SquareButton onPress={this.props.onLeftButtonPress} />
            <View style={styles.square}>
                <View style={{ width: 70, height: 70, borderRadius: 70, backgroundColor: COLORS.DARKGREY }}></View>
            </View>
            <SquareButton onPress={this.props.onRightButtonPress} />
            <View style={[styles.square, { backgroundColor: 'transparent' }]}></View>
            <SquareButton onPress={this.props.onDownButtonPress} />
            <View style={[styles.square, { backgroundColor: 'transparent' }]}></View>
        </View>
    )
}

class SquareButton extends Component {
    render = () => (
        <TouchableOpacity
            onPress={this.props.onPress}>
            <View style={[styles.square, this.props.style]}></View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    square: {
        width: 70,
        height: 70,
        backgroundColor: COLORS.BLACK
    },
    container: {
        width: 210,
        height: 210,
        flexDirection: "row",
        flexWrap: "wrap"
    }
})

export default DirectionalStick