import { authConstants } from '../constants';

const initialState = { user: null, token: null };

export function auth(state = initialState, action) {
  switch (action.type) {
    case authConstants.AUTH_LOGIN:
      return {
        ...state,
        token: action.token
      }
    case authConstants.AUTH_UPDATE_USER:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.user
        }
      }
    case authConstants.AUTH_LOGOUT:
      return {
        ...state,
        user: null,
        token: null
      }
    default:
      return state
  }
}