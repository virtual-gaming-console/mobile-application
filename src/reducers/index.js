import { combineReducers } from "redux"
import { lobby } from './lobby'
import { auth } from './auth'

export const rootReducer = combineReducers({
  lobby,
  auth
})