import React, { Component } from "react"
import CONFIG from '../config'

function withSocket(WrappedComponent) {
  class withSocket extends Component {
    state = {
      socket: null
    }

    componentDidMount() {
      const socket = new WebSocket(CONFIG.WS_API_URL)
      this.setState({
        socket
      })
    }

    componentWillUnmount() {
      // this.state.socket.close()
    }

    render = () => <WrappedComponent socket={this.state.socket} {...this.props} />
  }

  withSocket.displayName = `withSocket(${getDisplayName(WrappedComponent)})`
  return withSocket
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default withSocket