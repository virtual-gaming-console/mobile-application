import CONFIG from '../config'

class LobbyService {
  static async check(code) {
    let req = await fetch(`${CONFIG.API_URL}/lobby/check/${code}`)
    return req.json()
  }
}

export default LobbyService