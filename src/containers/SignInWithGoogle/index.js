import React, { Component } from 'react'
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import * as Google from 'expo-google-app-auth'
import * as GoogleSignIn from 'expo-google-sign-in'
import CONFIG from '../../config'
import AuthService from '../../services/AuthService'
import { AsyncStorage } from 'react-native'
import { login, updateUser } from '../../actions/auth'
import { connect } from "react-redux"
import UserWrapper from '../UserWrapper'
import googleSignImage from '../../../assets/btn_google_signin.png'

class SignInWithGoogle extends Component {
  componentDidMount() {
    if (!__DEV__) {
      this.initAsync()
    } else {
      this.updateToken()
    }
  }

  initAsync = async () => {
    await GoogleSignIn.initAsync();
    const user = await GoogleSignIn.signInSilentlyAsync();
    this._syncUserWithStateAsync(user);
  }

  _syncUserWithStateAsync = async ({ uid, email }) => {
    const response = await AuthService.googleAuthCallback({ id: uid, email })
    if (response.success) {
      const token = response.token
      if (token) {
        await AsyncStorage.setItem('token', JSON.stringify({ token, timestamp: Date.now() + 1000 * 60 * 60 * 23 }))
        this.props.login(token)
      }
    }
  }

  async updateToken() {
    this.props.login(await AuthService.getToken())
  }

  componentDidUpdate(lastProps) {
    if (!lastProps.auth.token && this.props.auth.token !== null) {
      this.getMe()
    }
  }

  async getMe() {
    let response = await AuthService.getMe()

    if (response.success) {
      this.props.updateUser(response.data)
      if (!response.data.nickname) {
        this.props.onUserUnknown()
      }
    }
  }

  signIn = async () => {
    if (__DEV__) {
      try {
        const result = await Google.logInAsync({
          androidClientId: CONFIG.GOOGLE_CLIENT_ID,
          scopes: ["profile", "email"]
        })
        if (result.type === "success") {
          this._syncUserWithStateAsync(result.user);
        } else {
          console.log("cancelled")
        }
      } catch (e) {
        console.log("error", e)
      }
    } else {
      try {
        await GoogleSignIn.askForPlayServicesAsync();
        const { type } = await GoogleSignIn.signInAsync();
        if (type === 'success') {
          const user = await GoogleSignIn.signInSilentlyAsync();
          this._syncUserWithStateAsync(user);
        }
      } catch ({ message }) {
        alert('login: Error:' + message);
      }
    }
  }

  render = () => {
    if (this.props.auth.user) {
      return this.props.auth.user.nickname ? <UserWrapper onPress={this.props.onPressUserWrapper} /> : null
    } else {
      return (
        <View>
          <TouchableOpacity onPress={this.signIn}>
            <Image
              style={styles.button}
              source={googleSignImage}
            />
          </TouchableOpacity>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 160
  }
})

const mapStateToProps = ({ auth }) => ({
  auth
})

const mapDispatchToProps = dispatch => ({
  login: token => dispatch(login(token)),
  updateUser: user => dispatch(updateUser(user)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInWithGoogle)