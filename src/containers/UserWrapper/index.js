import React, { Component } from 'react'
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { connect } from "react-redux"
import { updateUser } from '../../actions/auth'
import withSocket from '../../hoc/withSocket'
import coinImage from '../../../assets/coin.png'
import COLORS from '../../../colors'

class UserWrapper extends Component {
  getWrapper = () => (
    <View style={[styles.container]}>
      {this.props.auth.user.boxes ? <Text style={styles.eggPin}>{this.props.auth.user.boxes}</Text> : null}
      <Image source={{ uri: this.props.auth.user.avatarImage }} style={[styles.avatarImage]} />
      <View style={[styles.nicknameAndCoins]}>
        <Text style={[styles.nickname]}>{this.props.auth.user.nickname}</Text>
        <View style={[styles.coinsContainer]}>
          <Text style={[styles.coins]}>{this.props.auth.user.coins}</Text>
          <Image source={coinImage} style={[styles.coinImage]} />
        </View>
      </View>
    </View>
  )

  componentDidUpdate(prevProps) {
    if (!prevProps.socket && this.props.socket !== null) {
      this.initSocketEvents()
    }
  }

  initSocketEvents = () => {
    this.props.socket.onmessage = evt => {
      const data = JSON.parse(evt.data);
      switch (data.title) {
        case 'user-updated':
          this.userUpdated(data.data)
          break
        default:
          console.log('Unknown message in Header: ' + data.title)
      }
    }
    this.props.socket.onopen = () => {
      this.userConnect()
    }
  }

  userConnect = () => {
    const { user } = this.props.auth

    if (user) {
      this.props.socket.send(JSON.stringify({ title: 'user-connected', data: user }))
    }
  }

  userUpdated = data => {
    this.props.updateUser(data.user)
  }

  render = () => {
    if (!this.props.auth.user) {
      return false
    }

    if (this.props.onPress) {
      return (
        <TouchableOpacity onPress={this.props.onPress}>
          {this.getWrapper()}
        </TouchableOpacity>
      )
    } else {
      return this.getWrapper()
    }
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'relative'
  },
  eggPin: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    fontSize: 12,
    fontWeight: "700",
    borderRadius: 5,
    padding: 5,
    backgroundColor: COLORS.BG_MAIN,
    color: COLORS.WHITE,
    zIndex: 10
  },
  coinsContainer: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  nicknameAndCoins: {
    marginLeft: 5,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  avatarImage: {
    height: 50,
    width: 50
  },
  coinImage: {
    height: 12,
    width: 12,
    marginLeft: 2
  },
  nickname: {
    fontSize: 20,
    fontWeight: '700',
  },
  coins: {
    fontSize: 12,
    fontWeight: '600',
    flex: 1,
    textAlign: "right"
  }
})

const mapStateToProps = ({ auth }) => ({
  auth
})

const mapDispatchToProps = dispatch => ({
  updateUser: user => dispatch(updateUser(user)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSocket(UserWrapper))