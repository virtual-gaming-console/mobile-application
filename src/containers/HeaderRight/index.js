import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import SignInWithGoogle from '../SignInWithGoogle'
import { connect } from "react-redux"
import COLORS from '../../../colors'

class HeaderRight extends Component {
  completedQuestsCount() {
    const { user } = this.props.auth
    const { dailys } = user
    const { quests } = dailys

    return quests ? quests.filter(quest => quest.status === 'QUEST_STATUS_COMPLETED').length : 0
  }

  render = () => {
    return (
      <View style={[styles.container]}>
        {this.props.auth.user && this.props.auth.user.dailys && (
          <TouchableOpacity onPress={this.props.onPressQuestWrapper}>
            <View style={styles.questTab}>
              {this.completedQuestsCount() ? <Text style={styles.questPin}>{this.completedQuestsCount()}</Text> : null}
              <Text style={styles.questText}>QUESTS</Text>
            </View>
          </TouchableOpacity>
        )}
        <View style={{ width: 15 }} />
        <SignInWithGoogle onPressUserWrapper={this.props.onPressUserWrapper} onUserUnknown={this.props.onUserUnknown} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginRight: 15,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  questTab: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  questText: {
    fontSize: 25,
    fontWeight: "700",
    // color: COLORS.BLACK
  },
  questPin: {
    position: 'absolute',
    bottom: -6,
    left: -15,
    fontSize: 12,
    fontWeight: "700",
    borderRadius: 5,
    padding: 5,
    backgroundColor: COLORS.BG_MAIN,
    color: COLORS.WHITE,
    zIndex: 10
  }
})

const mapStateToProps = ({ auth }) => ({
  auth
})

export default connect(
  mapStateToProps
)(HeaderRight)