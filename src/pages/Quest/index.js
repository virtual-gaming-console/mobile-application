import React, { Component } from 'react'
import { StyleSheet, Text, View, ScrollView, Button, ImageBackground } from 'react-native'
import { connect } from "react-redux"
import { updateUser, logout } from '../../actions/auth'
import COLORS from '../../../colors'
import AuthService from '../../services/AuthService'
import HeaderRight from '../../containers/HeaderRight'
import boxImage from '../../../assets/egg.png'
import coinImage from '../../../assets/coin.png'

const QUEST_STATUS = {
  PENDING: "QUEST_STATUS_PENDING",
  COMPLETED: "QUEST_STATUS_COMPLETED",
  CLAIMED: "QUEST_STATUS_CLAIMED",
}

class Quest extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'MY QUESTS',
      headerRight: () => <HeaderRight onPressUserWrapper={() => navigation.navigate('Profile')} />
    }
  }

  state = {
    timeLeft: null,
    busy: false
  }

  computeTimeLeft = () => {
    let time = this.state.timeLeft
    let hours = Math.floor(time / 3600);
    time = time - hours * 3600;
    let minutes = Math.floor(time / 60);
    let seconds = time - minutes * 60;

    return {
      hours,
      minutes,
      seconds
    }
  }

  componentDidMount() {
    this.setState({
      timeLeft: this.props.auth.user.dailys.resetTime
    })
    this.inverval = setInterval(() => {
      this.setState(prevState => ({
        timeLeft: prevState.timeLeft - 1
      }))
    }, 1000)
  }

  componentWillMount() {
    clearInterval(this.inverval)
  }

  claimQuestRewards = async (quest) => {
    this.setState({
      busy: true
    }, async () => {
      let response = await AuthService.claimQuest(quest)

      if (response.success) {
        this.setState({
          busy: false
        })

        const { unlockedAvatars, ...user } = response.data
        this.props.updateUser(user)
      }
    })
  }

  render = () => {
    const computedTimeLeft = this.computeTimeLeft()

    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1, alignSelf: 'stretch' }}>
          {this.props.auth.user.dailys.quests.map((quest, index) => (
            <View key={index} style={styles.questItemWrapper}>
              <ImageBackground
                style={styles.questItemReward}
                imageStyle={styles.questItemRewardImage}
                source={quest.reward.boxes ? boxImage : coinImage}>
                <Text style={styles.questItemRewardText}>{quest.reward.boxes ? quest.reward.boxes : quest.reward.coins}</Text>
              </ImageBackground>
              <View style={styles.questItemDescription}>
                <Text style={styles.questItemTitle}>{quest.relatedGame !== undefined && `[${quest.relatedGame.title.toUpperCase()}] - `}{quest.title}</Text>
                <Text style={styles.questItemText}>{quest.text}</Text>
              </View>
              <View style={styles.questItemButtons}>
                {quest.status === QUEST_STATUS.COMPLETED && (
                  <Button onPress={() => this.claimQuestRewards(quest)} color={COLORS.BLACK} title="CLAIM" disabled={this.state.busy} />
                )}
                {quest.status === QUEST_STATUS.CLAIMED && (
                  <Button title="CLAIMED" disabled={true} />
                )}
                {quest.status === QUEST_STATUS.PENDING && (
                  <Text style={styles.questItemText}>You did it {quest.count} times for now !</Text>
                )}
              </View>
            </View>
          ))}
        </ScrollView>
        <View style={[styles.inline, styles.padding, { justifyContent: 'flex-end', alignSelf: 'stretch' }]}>
          <Text style={styles.timeText}>{computedTimeLeft.hours}:{computedTimeLeft.minutes}:{computedTimeLeft.seconds}</Text>
          <View style={{ width: 10 }} />
          <Button title="CANCEL" onPress={() => this.props.navigation.pop()} color={COLORS.BLACK}></Button>
        </View>
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_MAIN,
    padding: 20,
  },
  inline: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  padding: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
  },
  questItemWrapper: {
    alignSelf: 'stretch',
    padding: 15,
    backgroundColor: COLORS.WHITE,
    flexDirection: 'row',
    alignItems: "center",
    height: 100,
    marginBottom: 10
  },
  questItemReward: {
    backgroundColor: COLORS.BG_SUB,
    height: '100%',
    aspectRatio: 1,
    marginRight: 15,
    alignItems: "flex-end",
    justifyContent: "flex-end",
    borderRadius: 5
  },
  questItemRewardImage: {
    resizeMode: 'contain',
  },
  questItemRewardText: {
    fontSize: 16,
    color: COLORS.WHITE,
    fontWeight: "700",
    backgroundColor: "black",
    paddingLeft: 4,
    paddingRight: 4,
  },
  questItemDescription: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'stretch',
  },
  questItemTitle: {
    fontSize: 25,
    fontWeight: "700",
  },
  questItemText: {
    fontSize: 20,
  },
  questItemButtons: {
    alignSelf: 'stretch',
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  timeText: {
    fontSize: 20,
    color: COLORS.WHITE,
    fontWeight: "700",
    padding: 5,
    backgroundColor: COLORS.BG_SUB,
    borderRadius: 5
  }
});

const mapStateToProps = ({ auth }) => ({
  auth
})

const mapDispatchToProps = dispatch => ({
  updateUser: user => dispatch(updateUser(user)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Quest)
