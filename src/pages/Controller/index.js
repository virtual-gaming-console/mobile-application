import React, { Component } from 'react'
import { StyleSheet, View, Button, Text, TextInput, Image, Linking, AppState } from 'react-native'
import { connect } from "react-redux"
import COLORS from '../../../colors'
import ActionButton from '../../presentationals/ActionButton'
import DirectionalStick from '../../presentationals/DirectionalStick'
import withSocket from '../../hoc/withSocket'
import MenuButton from '../../presentationals/MenuButton'
import HeaderRight from '../../containers/HeaderRight'

class Controller extends Component {
  state = {
    openingController: false,
    nickname: {
      value: '',
      isValid: false
    },
    member: null
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.socket && this.props.socket !== null) {
      this.initSocketEvents()
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({ leaveLobby: () => this.lobbyDisconnect() });
    this.props.navigation.setParams({ beforeLogout: () => this.lobbyDisconnect() });
    AppState.addEventListener('change', this.handleAppStateChange)

    // this.props.navigation.addListener(
    //   'willBlur',
    //   () => {
    //     this.lobbyDisconnect()
    //   }
    // )

    if (this.props.auth.user) {
      this.setState({ nickname: { value: this.props.auth.user.nickname, isValid: true } })
    }
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
    this.props.navigation.setParams({ leaveLobby: undefined });
    this.props.navigation.setParams({ beforeLogout: undefined });
    // this.lobbyDisconnect()
  }

  handleAppStateChange = (nextAppState) => {
    if (nextAppState.match(/inactive|background/) && !this.state.openingController) {
      this.lobbyDisconnect();
    } else {
      this.setState({ openingController: false })
    }
  }

  lobbyDisconnect() {
    const { params } = this.props.navigation.state;
    const { lobbyCode } = params

    try {
      this.props.socket.send(JSON.stringify({ title: 'lobby-disconnect', data: { code: lobbyCode, member: this.state.member } }))
    } catch (e) {
      console.log(e.message)
    }

    this.props.navigation.pop()
  }

  initSocketEvents = () => {
    const { params } = this.props.navigation.state;
    const { lobbyCode } = params

    this.props.socket.onmessage = evt => {
      const data = JSON.parse(evt.data);
      switch (data.title) {
        case 'lobby-connect-success':
          this.onLobbyConnectSuccess(data.data)
          break
        case 'lobby-member-updated':
          this.onLobbyMemberUpdated(data.data)
          break
        case 'screen-action':
          this.triggerScreenAction(data.data)
          break
        default:
          console.log('Unknown message: ' + data.title)
      }
    }
    this.props.socket.onopen = () => {
      this.props.socket.send(JSON.stringify({ title: 'lobby-connect', data: { code: lobbyCode, id: this.props.auth.user ? this.props.auth.user.id : null } }))
    }
  }

  onNicknameChangeText = text => {
    this.setState(prevState => ({
      nickname: {
        ...prevState.nickname,
        value: text
      }
    }))
  }

  validateNickname = () => {
    const { params } = this.props.navigation.state;
    const { lobbyCode } = params

    this.setState(prevState => ({
      nickname: {
        ...prevState.nickname,
        isValid: true
      }
    }), () => {
      try {
        this.props.socket.send(JSON.stringify({ title: 'lobby-update-member', data: { code: lobbyCode, id: this.state.member.id, nickname: this.state.nickname.value } }))
      } catch (e) {
        console.log(e.message)
      }
    })
  }

  onLobbyConnectSuccess = data => {
    this.setState({
      member: data.member
    })
  }

  onLobbyMemberUpdated = data => {
    if (!this.props.auth.user) {
      this.setState({
        member: data.member
      })
      this.props.navigation.setParams({ member: data.member });
    }
  }

  sendAction = action => {
    try {
      this.props.socket.send(JSON.stringify({ title: 'controller-action', data: { memberId: this.state.member.id, action } }))
    } catch (e) {
      console.log(e.message)
    }
  }

  triggerScreenAction = data => {
    if (data.action === "open-controller") {
      this.tryOpenController(data.content);
    }
  }

  tryOpenController = url => {
    const { params } = this.props.navigation.state;
    const { lobbyCode } = params

    this.setState(
      { openingController: true },
      () => Linking.openURL(`${url}/#lobby_code=${lobbyCode}&playerId=${this.state.member.id}`)
    )
  }

  render = () => (
    <View style={styles.container}>
      {this.state.nickname.isValid ? (
        <>
          <View style={styles.menuContainer}>
            <MenuButton onPress={() => this.sendAction('menu')} />
          </View>
          <View style={styles.leftSide}>
            <DirectionalStick
              onRightButtonPress={() => this.sendAction('right')}
              onLeftButtonPress={() => this.sendAction('left')}
              onUpButtonPress={() => this.sendAction('up')}
              onDownButtonPress={() => this.sendAction('down')} />
          </View>
          <View style={styles.rightSide}>
            <ActionButton title="B" color={COLORS.RED} style={{ marginTop: 50, marginRight: 20 }} onPress={() => this.sendAction('nok')} />
            <ActionButton title="A" color={COLORS.GREEN} style={{ marginBottom: 50 }} onPress={() => this.sendAction('ok')} />
          </View>
        </>
      ) : (
          <View style={styles.full}>
            <Text style={styles.subtitle}>CHOOSE YOUR NICKNAME</Text>
            <TextInput style={styles.input}
              placeholder="XxDarkSasukexX"
              placeholderTextColor={COLORS.DARKGREY}
              onChangeText={this.onNicknameChangeText}
              value={this.state.nickname.value}
              autoCorrect={false} />
            <Button color={COLORS.BLACK} title="VALIDATE" onPress={this.validateNickname}></Button>
          </View>
        )}

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_MAIN,
    position: "relative",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  leftSide: {
    width: '45%',
    // height: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30
  },
  rightSide: {
    width: '45%',
    // height: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30
  },
  full: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    display: 'flex'
  },
  subtitle: {
    fontSize: 25,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.WHITE,
    padding: 20
  },
  input: {
    fontSize: 22,
    fontWeight: '700',
    padding: 20,
    backgroundColor: COLORS.WHITE,
    color: COLORS.BLACK,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    minWidth: 250,
    marginBottom: 25
  },
  menuContainer: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5
  }
})

const ConnectedController = withSocket(Controller);

ConnectedController.getNamePanel = navigation => {
  const member = navigation.getParam('member', null)
  const leaveLobby = navigation.getParam('leaveLobby', null)

  if (member && member.nickname && member.picture) {
    return (
      <View style={{ display: 'flex', flex: 1, alignItems: 'center', flexDirection: 'row', marginRight: 20 }}>
        <Image source={{ uri: member.picture }} style={{ height: 50, width: 50 }} />
        <Text style={{ marginLeft: 5, fontSize: 20, fontWeight: '700' }}>{member.nickname}</Text>
      </View>
    );
  } else {
    return <HeaderRight
      onPressUserWrapper={() => navigation.push('Profile', { beforeLogout: () => leaveLobby() })}
      onUserUnknown={() => navigation.push('UpdateNickname')}
      onPressQuestWrapper={() => navigation.push('Quest')} />
  }
}

ConnectedController.getLeftHeader = navigation => {
  const leaveLobby = navigation.getParam('leaveLobby', null)

  return (
    <View style={{ display: 'flex', flex: 1, alignItems: 'center', flexDirection: 'row', marginLeft: 20 }}>
      <Button title="LEAVE LOBBY" onPress={() => leaveLobby()} />
      <Text style={{ marginLeft: 20, fontSize: 20, fontWeight: '700' }}>CONTROLLER</Text>
    </View>
  )
}

ConnectedController.navigationOptions = ({ navigation, navigationOptions }) => {
  return {
    headerLeft: () => ConnectedController.getLeftHeader(navigation),
    headerRight: () => ConnectedController.getNamePanel(navigation)
  }
};

const mapStateToProps = ({ auth }) => ({
  auth
})

export default connect(
  mapStateToProps
)(ConnectedController)