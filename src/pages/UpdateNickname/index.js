import React, { Component } from 'react'
import { StyleSheet, Text, TextInput, View, Button, Image } from 'react-native'
import { connect } from "react-redux"
import { updateUser } from '../../actions/auth'
import COLORS from '../../../colors'
import SignInWithGoogle from '../../containers/SignInWithGoogle'
import AuthService from '../../services/AuthService'
import errorImage from '../../../assets/error.png'

class UpdateNickname extends Component {
  static navigationOptions = {
    title: 'Nickname update',
    headerRight: () => <SignInWithGoogle />
  }

  state = {
    nickname: "",
    error: null
  }

  componentDidMount() {
    this.setState({
      nickname: this.props.auth.user.nickname
    })
  }

  onNicknameChangeText = text => {
    this.setState({
      nickname: text,
      error: null
    })
  }

  validateNickname = async () => {
    let response = await AuthService.updateProfile({ nickname: this.state.nickname })

    if (response.success) {
      this.props.updateUser(response.data)
      this.props.navigation.goBack()
    } else {
      this.setState({
        error: response.message
      })
    }
  }

  render = () => (
    <View style={styles.container}>
      <Text style={styles.subtitle}>ENTER A POWERFULL NICKNAME !</Text>
      <TextInput style={styles.input}
        placeholder="XxDarkSasukexX"
        placeholderTextColor={COLORS.DARKGREY}
        onChangeText={this.onNicknameChangeText}
        value={this.state.nickname}
        autoCorrect={false} />
      <Button color={COLORS.BLACK} title="VALIDATE" onPress={this.validateNickname}></Button>
      <View style={styles.spacer} />
      {this.state.error && (
        <View style={styles.error}>
          <Image source={errorImage} style={styles.errorImage} />
          <Text style={styles.errorText}>{this.state.error}</Text>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_MAIN,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    display: 'flex',
    padding: 20,
  },
  subtitle: {
    fontSize: 25,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.WHITE,
    padding: 20
  },
  input: {
    fontSize: 22,
    fontWeight: '700',
    padding: 20,
    backgroundColor: COLORS.WHITE,
    color: COLORS.BLACK,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    minWidth: 250,
    marginBottom: 25
  },
  spacer: {
    flex: 1
  },
  errorImage: {
    width: 30,
    height: 30,
    marginRight: 8
  },
  error: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 12,
    paddingRight: 12,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: COLORS.WHITE,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  errorText: {
    fontSize: 22,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.RED,
  }
});

const mapStateToProps = ({ auth }) => ({
  auth
})

const mapDispatchToProps = dispatch => ({
  updateUser: user => dispatch(updateUser(user)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateNickname)
