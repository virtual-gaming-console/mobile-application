import React, { Component } from 'react'
import { Image, StyleSheet, Text, View, ScrollView, Button, TouchableWithoutFeedback, ImageBackground } from 'react-native'
import { connect } from "react-redux"
import { updateUser, logout } from '../../actions/auth'
import COLORS from '../../../colors'
import AuthService from '../../services/AuthService'
import boxImage from '../../../assets/egg.png'
import starImage from '../../../assets/star.png'
import greyStarImage from '../../../assets/star-grey.png'
import coinImage from '../../../assets/coin.png'
import Config from '../../config'
import { SafeAreaView } from 'react-navigation'
import HeaderRight from '../../containers/HeaderRight'

const BOXCOST = 500
const ACTIONS = {
  BUYING_BOXES: 'BUYING_BOXES',
  OPENING_BOXES: 'OPENING_BOXES',
  FUSING_AVATAR: 'FUSING_AVATAR',
  SELLING_AVATAR: 'SELLING_AVATAR',
}

class Inventory extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'MY INVENTORY',
      headerRight: () => <HeaderRight onPressQuestWrapper={() => navigation.navigate('Quest')} />
    }
  }

  state = {
    itemSelected: null,
    inventory: null,
    busy: false,
    currentAction: null,
    loots: null,
    shinyMode: false,
    fusingList: [null, null],
    sellingList: []
  }

  componentDidMount() {
    this.getInventory()
  }

  getInventory = async () => {
    let response = await AuthService.getInventory()

    if (response.success) {
      this.setState({
        inventory: response.data
      })
    }
  }

  buyBoxes = async quantity => {
    this.setState({
      busy: true
    }, async () => {
      let response = await AuthService.buyBox(quantity)

      if (response.success) {
        this.setState({
          busy: false,
          currentAction: response.data.coins < BOXCOST ? null : this.state.currentAction
        })

        const { unlockedAvatars, ...user } = response.data
        this.props.updateUser(user)
      }
    })

  }

  openBoxes = async quantity => {
    this.setState({
      busy: true
    }, async () => {
      let response = await AuthService.openBox(quantity)

      if (response.success) {
        this.setState({
          loots: {
            avatars: response.data.loots
          },
          inventory: response.data.inventory,
          busy: false,
          currentAction: response.data.user.boxes < 1 ? null : this.state.currentAction
        })

        const { unlockedAvatars, ...user } = response.data.user
        this.props.updateUser(user)
      }
    })
  }

  unlockAvatar = () => {
    if (!this.avatarAlreadyUnlocked(this.state.itemSelected.id)) {
      this.setState({
        busy: true
      }, async () => {
        let response = await AuthService.unlockAvatar(this.state.itemSelected.id, this.state.shinyMode)

        if (response.success) {
          let itemSelected = this.state.itemSelected
          let shinyMode = this.state.shinyMode
          if (this.getRawSelectedItem().quantity + this.getRawSelectedItem().shinyQuantity === 1) {
            itemSelected = null
            shinyMode = false
          } else if (this.getRawSelectedItem().shinyQuantity === 1 && this.state.shinyMode) {
            shinyMode = false
          } else if (this.getRawSelectedItem().quantity === 1 && !this.state.shinyMode) {
            shinyMode = true
          }

          this.setState({
            inventory: response.data.inventory,
            itemSelected,
            shinyMode,
            busy: false,
            loots: {
              avatars: [{ avatar: response.data.unlockedAvatar, isShiny: response.data.unlockedAvatar.shiny }]
            }
          })
          this.props.updateUser(response.data.user)
        }
      })
    }
  }

  fuseAvatar = () => {
    if (this.fusingListIsFull()) {
      this.setState({
        busy: true
      }, async () => {
        let response = await AuthService.fuseAvatar(
          this.state.itemSelected.id,
          this.state.fusingList.filter(x => x && x.isShiny).length,
          this.state.fusingList.filter(x => x && !x.isShiny).length
        )

        if (response.success) {
          this.setState({
            inventory: response.data.inventory,
            itemSelected: response.data.inventory.ownedAvatars.find(x => x.avatar.id === response.data.fusedAvatar.avatarId).avatar,
            shinyMode: response.data.isShiny,
            currentAction: null,
            fusingList: [null, null],
            sellingList: [],
            busy: false
          })
        }
      })
    }
  }

  sellAvatar = () => {
    this.setState({
      busy: true
    }, async () => {
      let response = await AuthService.sellAvatar(
        this.state.itemSelected.id,
        this.state.sellingList.filter(x => x && x.isShiny).length,
        this.state.sellingList.filter(x => x && !x.isShiny).length
      )

      if (response.success) {
        let itemSelected = this.state.itemSelected
        let shinyMode = this.state.shinyMode
        if (response.data.inventory.ownedAvatars.find(x => x.avatar.id === this.state.itemSelected.id) === undefined) {
          itemSelected = null
          shinyMode = false
        } else {
          const rawItem = response.data.inventory.ownedAvatars.find(x => x.avatar.id === this.state.itemSelected.id)
          if (rawItem.shinyQuantity <= 0 && shinyMode) {
            shinyMode = false
          } else if (rawItem.quantity <= 0 && !shinyMode) {
            shinyMode = true
          }
        }

        this.setState({
          inventory: response.data.inventory,
          busy: false,
          itemSelected,
          shinyMode,
          currentAction: null,
          fusingList: [null, null],
          sellingList: [],
          // loots: {
          //   coins: response.data.money
          // }
        })

        const { unlockedAvatars, ...user } = response.data.user
        this.props.updateUser(user)
      }
    })
  }

  handleClickBox = () => {
    if (!this.isSelectedItem('box')) {
      this.setState({
        itemSelected: 'box',
        shinyMode: false,
        currentAction: null,
        fusingList: [null, null],
        sellingList: [],
        loots: null,
      })
    }
  }

  isSelectedItem = item => {
    return this.state.itemSelected !== null && (item === this.state.itemSelected || (item.id !== undefined && item.id === this.state.itemSelected.id))
  }

  getItemStyle = item => {
    let style = {}
    let opacity = '.8'
    if (item !== null) {
      if (this.isSelectedItem(item)) {
        style.borderColor = COLORS.WHITE
        opacity = '.9'
      }
      if (item.isLegendary) {
        style.backgroundColor = 'rgba(201, 176, 55, ' + opacity + ')'
      } else {
        switch (item.rank) {
          case 3:
            style.backgroundColor = 'rgba(215, 215, 215, ' + opacity + ')'
            break
          case 2:
            style.backgroundColor = 'rgba(173, 138, 86, ' + opacity + ')'
            break
          default:
            style.backgroundColor = 'rgba(0,0,0,' + opacity + ')'
            break
        }
      }
    } else {
      style.borderColor = COLORS.WHITE
      style.backgroundColor = 'rgba(0,0,0,' + opacity + ')'
    }

    return style
  }

  getSortedAvatarList = () => {
    return this.state.inventory ? this.state.inventory.ownedAvatars.sort((a, b) => a.avatar.index - b.avatar.index) : []
  }

  handleClickAvatar = avatar => {
    const avatarModel = avatar.avatar
    if (!this.isSelectedItem(avatarModel)) {

      this.setState({
        itemSelected: avatarModel,
        shinyMode: avatar.quantity <= 0,
        currentAction: null,
        fusingList: [null, null],
        sellingList: [],
        loots: null,
      })
    }
  }

  getAvatarImageUrl = (avatarModel, isShiny) => {
    if (!avatarModel) {
      return null
    } else if (avatarModel && new RegExp(Config.API_URL).test(avatarModel.image)) {
      return avatarModel.image
    } else {
      return `${Config.API_URL}/assets/images/avatars/${isShiny ? avatarModel.shinyImage : avatarModel.image}`
    }
  }

  avatarItemWrapper = ({ avatar, style, onPress, counters }) => {
    const avatarModel = avatar ? avatar.avatar : null

    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <ImageBackground
          style={[styles.itemWrapper, this.getItemStyle(avatarModel), style]}
          imageStyle={styles.itemImage}
          source={{ uri: this.getAvatarImageUrl(avatarModel, avatar && avatar.isShiny) }}>
          <View style={styles.avatarCountWrapper}>
            {avatar && !counters && (
              <View style={styles.avatarCountContainer}>
                <Image style={styles.avatarItemStar} source={avatar.isShiny ? starImage : greyStarImage} alt="star_img" />
              </View>
            )}
            {avatar && counters && counters.shiny !== undefined && (
              <View style={styles.avatarCountContainer}>
                <Text style={styles.avatarCountText}>{counters.shiny}</Text>
                <Image style={styles.avatarItemStar} source={starImage} alt="star_img" />
              </View>
            )}
            {avatar && counters && counters.normal !== undefined && (
              <View style={styles.avatarCountContainer}>
                <Text style={styles.avatarCountText}>{counters.normal}</Text>
                <Image style={styles.avatarItemStar} source={greyStarImage} alt="star_img" />
              </View>
            )}
          </View>
        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }

  getSelectedItemName = () => {
    if (!this.state.itemSelected) {
      return null
    } else if (this.state.itemSelected === 'box') {
      return "Egg"
    } else {
      return (this.state.shinyMode ? 'Shiny ' : '') + this.state.itemSelected.names.en
    }
  }

  getSelectedItemImage = () => {
    if (!this.state.itemSelected) {
      return null
    } else if (this.state.itemSelected === 'box') {
      return boxImage
    } else if (this.state.currentAction === ACTIONS.FUSING_AVATAR) {
      return { uri: this.getAvatarImageUrl({ ...this.state.itemSelected, image: this.state.itemSelected.evolution }, false) }
    } else {
      return { uri: this.getAvatarImageUrl(this.state.itemSelected, this.state.shinyMode) }
    }
  }

  avatarAlreadyUnlocked = avatarId => {
    const avatars = this.props.auth.user.unlockedAvatars.filter(avatarItem => avatarItem.id === avatarId)
    if (avatars.length === 0) {
      return false
    } else {
      return avatars.find(x => x.shiny === this.state.shinyMode) !== undefined
    }
  }

  getItemButtonWrapper = () => {
    if (!this.state.itemSelected) {
      return null
    } else if (this.state.itemSelected === 'box') {
      return (
        <View style={styles.buttonsWrapper}>
          {this.state.currentAction === ACTIONS.BUYING_BOXES && (
            <>
              <View style={{ width: 5 }} />
              <Button title="BUY 1" onPress={() => this.onPressBuyButtonHandler(1)} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.coins < BOXCOST || this.state.busy}></Button>
              <View style={{ width: 20 }} />
              <Button title="BUY 5" onPress={() => this.onPressBuyButtonHandler(5)} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.coins < BOXCOST * 5 || this.state.busy}></Button>
              <View style={{ width: 20 }} />
              <Button title="BUY 10" onPress={() => this.onPressBuyButtonHandler(10)} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.coins < BOXCOST * 10 || this.state.busy}></Button>
            </>
          )}
          {this.state.currentAction === ACTIONS.OPENING_BOXES && (
            <>
              <View style={{ width: 5 }} />
              <Button title="OPEN 1" onPress={() => this.onPressOpenButtonHandler(1)} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.boxes <= 0 || this.state.busy}></Button>
              <View style={{ width: 20 }} />
              <Button title="OPEN 5" onPress={() => this.onPressOpenButtonHandler(5)} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.boxes < 5 || this.state.busy}></Button>
              <View style={{ width: 20 }} />
              <Button title="OPEN 10" onPress={() => this.onPressOpenButtonHandler(10)} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.boxes < 10 || this.state.busy}></Button>
            </>
          )}
          <View style={{ flex: 1, height: 20 }} />
          <Button title={`${this.state.currentAction === ACTIONS.BUYING_BOXES ? 'x ' : ''}BUY`} onPress={() => this.onPressBuyButtonHandler()} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.coins < BOXCOST}></Button>
          <View style={{ width: 20 }} />
          <Button title={`${this.state.currentAction === ACTIONS.OPENING_BOXES ? 'x ' : ''}OPEN`} onPress={() => this.onPressOpenButtonHandler()} style={styles.button} color={COLORS.BLACK} disabled={this.props.auth.user.boxes <= 0}></Button>
          <View style={{ width: 5 }} />
        </View>
      )
    } else {
      const avatar = this.state.inventory.ownedAvatars.find(x => x.avatar.id === this.state.itemSelected.id)
      const avatarModel = this.state.itemSelected

      const unlockButton = { title: "UNLOCK" }
      if (this.state.busy || (avatar.quantity === 0 && !this.state.shinyMode) || (avatar.shinyQuantity === 0 && this.state.shinyMode)) {
        unlockButton.disabled = true
      }
      if (this.avatarAlreadyUnlocked(avatarModel.id)) {
        unlockButton.disabled = true
        unlockButton.title = "UNLOCKED"
      }

      const completeButton = { disabled: false }
      if (this.state.currentAction === ACTIONS.FUSING_AVATAR) {
        completeButton.disabled = !this.fusingListIsFull() || this.state.busy
      }
      if (this.state.currentAction === ACTIONS.SELLING_AVATAR) {
        completeButton.disabled = this.sellingListIsEmpty() || this.state.busy
      }

      return (
        <View style={styles.buttonsWrapper}>
          {(this.state.currentAction === ACTIONS.FUSING_AVATAR || this.state.currentAction === ACTIONS.SELLING_AVATAR) && (
            <>
              <View style={{ width: 5 }} />
              <Button title="CANCEL" onPress={this.onPressCancelButtonHandler} style={styles.button} color={COLORS.BLACK} disabled={this.state.busy}></Button>
              <View style={{ width: 20 }} />
              <Button title="COMPLETE" onPress={this.onPressCompleteFuseButtonHandler} style={styles.button} color={COLORS.BLACK} {...completeButton}></Button>
            </>
          )}
          <View style={{ flex: 1, height: 20 }} />
          <Button {...unlockButton} onPress={this.onPressUnlockButtonHandler} style={styles.button} color={COLORS.BLACK}></Button>
          {this.state.itemSelected.evolution && (
            <>
              <View style={{ width: 20 }} />
              <Button title={`${this.state.currentAction === ACTIONS.FUSING_AVATAR ? 'x ' : ''}FUSE`} onPress={this.onPressFuseButtonHandler} style={styles.button} color={COLORS.BLACK} disabled={(avatar.quantity + avatar.shinyQuantity) < 2 || this.state.busy}></Button>
            </>
          )}
          <View style={{ width: 20 }} />
          <Button title={`${this.state.currentAction === ACTIONS.SELLING_AVATAR ? 'x ' : ''}SELL`} onPress={this.onPressSellButtonHandler} style={styles.button} color={COLORS.BLACK} disabled={this.state.busy}></Button>
          <View style={{ width: 5 }} />
        </View>
      )
    }
  }

  onPressBuyButtonHandler = quantity => {
    if (quantity) {
      this.buyBoxes(quantity)
    } else {
      this.setState({
        currentAction: this.state.currentAction === ACTIONS.BUYING_BOXES ? null : ACTIONS.BUYING_BOXES
      })
    }
  }

  onPressOpenButtonHandler = quantity => {
    if (quantity) {
      this.openBoxes(quantity)
    } else {
      this.setState({
        currentAction: this.state.currentAction === ACTIONS.OPENING_BOXES ? null : ACTIONS.OPENING_BOXES
      })
    }
  }

  onPressUnlockButtonHandler = () => {
    this.unlockAvatar()
  }

  onPressFuseButtonHandler = () => {
    this.setState({
      currentAction: this.state.currentAction === ACTIONS.FUSING_AVATAR ? null : ACTIONS.FUSING_AVATAR,
      fusingList: [null, null],
      sellingList: []
    })
  }

  onPressCompleteFuseButtonHandler = () => {
    if (this.state.currentAction === ACTIONS.FUSING_AVATAR) {
      this.fuseAvatar()
    } else if (this.state.currentAction === ACTIONS.SELLING_AVATAR) {
      this.sellAvatar()
    }
  }

  onPressCancelButtonHandler = () => {
    this.setState({
      currentAction: null,
      fusingList: [null, null],
      sellingList: []
    })
  }

  onPressSellButtonHandler = () => {
    this.setState({
      currentAction: this.state.currentAction === ACTIONS.SELLING_AVATAR ? null : ACTIONS.SELLING_AVATAR,
      fusingList: [null, null],
      sellingList: []
    })
  }

  onPressToggleShinyButtonHandler = () => {
    this.setState({
      shinyMode: !this.state.shinyMode
    })
  }

  closeLootPanel = () => {
    this.setState({
      loots: null
    })
  }

  getRawSelectedItem = () => {
    return this.state.itemSelected && this.state.inventory ? this.state.inventory.ownedAvatars.find(x => x.avatar.id === this.state.itemSelected.id) : null
  }

  toggleShinyWrapper = () => {
    if (!this.state.loots && this.state.currentAction !== ACTIONS.FUSING_AVATAR && this.state.currentAction !== ACTIONS.SELLING_AVATAR && this.getRawSelectedItem()) {
      return (
        <View style={styles.shinyToggleWrapper}>
          <TouchableWithoutFeedback onPress={() => this.getRawSelectedItem().shinyQuantity > 0 && this.onPressToggleShinyButtonHandler()}>
            <ImageBackground style={[styles.itemWrapper, this.getItemStyle(this.state.itemSelected), { opacity: this.getRawSelectedItem().shinyQuantity > 0 ? 1 : 0.5, width: '9%' }]} imageStyle={styles.itemImage} source={{ uri: this.getAvatarImageUrl(this.state.itemSelected, true) }}>
              <View style={styles.avatarCountWrapper}>
                <View style={styles.avatarCountContainer}>
                  <Image style={styles.avatarItemStar} source={starImage} alt="star_img" />
                </View>
              </View>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </View>
      )
    }

    return null
  }

  avatarLootPanel = () => {
    if (this.state.loots && this.state.loots.avatars) {
      return (
        <View style={styles.lootPanel}>
          <View style={[styles.itemsList, { width: '70%', alignSelf: 'auto', justifyContent: this.state.loots.avatars.length === 1 ? 'center' : 'flex-start' }]}>
            {this.state.loots.avatars.map((avatar, index) => <this.avatarItemWrapper avatar={avatar} key={index} style={{ width: '18%' }} />)}
            {this.state.loots.avatars.length > 5 && this.state.loots.avatars.length % 5 !== 0 && (
              <View style={{ width: ((5 - this.state.loots.avatars.length % 5) * 18) + '%' }} />
            )}
          </View>
          <Button title="OK" onPress={this.closeLootPanel} color={COLORS.BLACK}></Button>
        </View>
      )
    }

    return null
  }

  removeAvatarFromFusingListAtIndex = index => {
    const nextFusingList = [...this.state.fusingList]
    nextFusingList[index] = null

    this.setState({
      fusingList: nextFusingList
    })
  }

  addAvatarToFusingList = isShiny => {
    const rawItem = this.getRawSelectedItem()
    const nextFusingList = [...this.state.fusingList]
    nextFusingList[nextFusingList[0] === null ? 0 : 1] = { ...rawItem, isShiny }

    this.setState({
      fusingList: nextFusingList
    })
  }

  fusingListIsFull = () => {
    return this.state.fusingList[0] !== null && this.state.fusingList[1] !== null
  }

  avatarFusePanel = () => {
    if (this.state.currentAction === ACTIONS.FUSING_AVATAR) {
      const rawItem = this.getRawSelectedItem()
      const actualShinyQuantity = rawItem.shinyQuantity - this.state.fusingList.filter(x => x && x.isShiny).length
      const actualQuantity = rawItem.quantity - this.state.fusingList.filter(x => x && !x.isShiny).length

      return (
        <View style={{ flex: 1, alignSelf: 'stretch', paddingBottom: 8 }}>
          <View style={[styles.itemsList, { width: '100%', alignSelf: 'auto', justifyContent: 'center' }]}>
            <this.avatarItemWrapper avatar={this.state.fusingList[0]} style={{ width: '12%' }} onPress={() => this.removeAvatarFromFusingListAtIndex(0)} />
            <View style={{ width: '10%' }} />
            <this.avatarItemWrapper avatar={this.state.fusingList[1]} style={{ width: '12%' }} onPress={() => this.removeAvatarFromFusingListAtIndex(1)} />
          </View>
          <View style={{ flex: 1 }} />
          <View style={[styles.itemsList, { width: '100%', alignSelf: 'auto', justifyContent: 'center' }]}>
            <this.avatarItemWrapper
              avatar={rawItem}
              style={{ width: '12%', opacity: actualQuantity > 0 && !this.fusingListIsFull() ? 1 : .6 }}
              counters={{ normal: actualQuantity }}
              onPress={() => (actualQuantity > 0 && !this.fusingListIsFull()) && this.addAvatarToFusingList(false)} />
            <View style={{ width: '10%' }} />
            <this.avatarItemWrapper
              avatar={{ ...rawItem, isShiny: true }}
              style={{ width: '12%', opacity: actualShinyQuantity > 0 && !this.fusingListIsFull() ? 1 : .6 }}
              counters={{ shiny: actualShinyQuantity }}
              onPress={() => (actualShinyQuantity > 0 && !this.fusingListIsFull()) && this.addAvatarToFusingList(true)} />
          </View>
        </View>
      )
    }

    return null
  }

  sellingListIsEmpty = () => {
    return this.state.sellingList.length === 0
  }

  sellingListIsFull = () => {
    return this.state.sellingList.length === 7
  }

  addAvatarToSellingList = isShiny => {
    const rawItem = this.getRawSelectedItem()
    const nextSellingList = [...this.state.sellingList, { ...rawItem, isShiny }]

    this.setState({
      sellingList: nextSellingList
    })
  }

  removeAvatarFromSellingListAtIndex = indexToRemove => {
    const nextSellingList = [...this.state.sellingList.filter((x, index) => index !== indexToRemove)]

    this.setState({
      sellingList: nextSellingList
    })
  }

  avatarSellPanel = () => {
    if (this.state.currentAction === ACTIONS.SELLING_AVATAR) {
      const rawItem = this.getRawSelectedItem()
      const actualShinyQuantity = rawItem.shinyQuantity - this.state.sellingList.filter(x => x && x.isShiny).length
      const actualQuantity = rawItem.quantity - this.state.sellingList.filter(x => x && !x.isShiny).length
      const fillingList = new Array(7 - this.state.sellingList.length % 7).fill(null)

      return (
        <View style={{ flex: 1, alignSelf: 'stretch', paddingBottom: 8 }}>
          <View style={[styles.itemsList]}>
            {this.state.sellingList.map((avatar, index) => <this.avatarItemWrapper avatar={avatar} key={index} style={{ width: '13%' }} onPress={() => this.removeAvatarFromSellingListAtIndex(index)} />)}
            {fillingList.map((x, index) => <View style={{ width: '13%' }} key={"void" + index} />)}
          </View>
          <View style={{ flex: 1 }} />
          <View style={styles.sellTextWrapper}>
            <Text style={styles.sellText}>SELLING FOR {this.state.sellingList.length * rawItem.cost}</Text>
            <Image source={coinImage} style={styles.sellImage} />
          </View>
          <View style={[styles.itemsList, { width: '100%', alignSelf: 'auto', justifyContent: 'center' }]}>
            <this.avatarItemWrapper
              avatar={rawItem}
              style={{ width: '12%', opacity: actualQuantity > 0 && !this.sellingListIsFull() ? 1 : .6 }}
              counters={{ normal: actualQuantity }}
              onPress={() => (actualQuantity > 0 && !this.sellingListIsFull()) && this.addAvatarToSellingList(false)} />
            <View style={{ width: '10%' }} />
            <this.avatarItemWrapper
              avatar={{ ...rawItem, isShiny: true }}
              style={{ width: '12%', opacity: actualShinyQuantity > 0 && !this.sellingListIsFull() ? 1 : .6 }}
              counters={{ shiny: actualShinyQuantity }}
              onPress={() => (actualShinyQuantity > 0 && !this.sellingListIsFull()) && this.addAvatarToSellingList(true)} />
          </View>
        </View>
      )
    }

    return null
  }

  titleWrapper = () => {
    return (
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>INVENTORY</Text>
        <View style={{ flex: 1 }} />
        {this.getSelectedItemName() && <Text style={styles.itemTitleText}>{this.getSelectedItemName()}</Text>}
      </View>
    )
  }

  leftWrapper = () => {
    const fillingList = new Array(3 - this.getSortedAvatarList().length % 3).fill(null)
    return (
      <View style={styles.leftWrapper}>
        <ScrollView style={{ flex: 1 }}>
          <Text style={styles.subtitle}>ITEMS</Text>
          <View style={styles.itemsList}>
            <TouchableWithoutFeedback onPress={() => this.handleClickBox()}>
              <ImageBackground style={[styles.itemWrapper, this.getItemStyle('box')]} imageStyle={styles.itemImage} source={boxImage}>
                <Text style={styles.boxCount}>{this.props.auth.user.boxes}</Text>
              </ImageBackground>
            </TouchableWithoutFeedback>
          </View>
          <Text style={styles.subtitle}>AVATAR SHARDS</Text>
          <View style={styles.itemsList}>
            {this.getSortedAvatarList().map((avatar, index) => (
              <this.avatarItemWrapper
                avatar={avatar}
                key={index}
                counters={{ shiny: avatar.shinyQuantity ? avatar.shinyQuantity : undefined, normal: avatar.quantity ? avatar.quantity : undefined }}
                onPress={() => this.handleClickAvatar(avatar)} />))}
            {fillingList.map((x, index) => <View style={{ width: '30%' }} key={"void" + index} />)}
          </View>
        </ScrollView>
        <View style={styles.leftButtonWrapper}>
          <Button title="CANCEL" onPress={() => this.props.navigation.pop()} color={COLORS.BLACK}></Button>
          <View style={{ flex: 1 }} />
        </View>
      </View>
    )
  }

  boxCostWrapper = () => {
    if (this.state.itemSelected !== 'box') {
      return null
    }

    return (
      <View style={styles.shinyToggleWrapper}>
        <View style={[styles.sellTextWrapper, { justifyContent: 'flex-end' }]}>
          <Text style={styles.sellText}>{BOXCOST}</Text>
          <Image source={coinImage} style={styles.sellImage} />
        </View>
      </View>
    )
  }

  rightWrapper = () => {
    return (
      <ImageBackground style={styles.rightWrapper} source={this.getSelectedItemImage()} imageStyle={{ resizeMode: "contain", margin: 20 }}>
        <View style={{ flex: 1, alignSelf: 'stretch' }}>
          <this.boxCostWrapper />
          <this.toggleShinyWrapper />
          <this.avatarLootPanel />
          <this.avatarFusePanel />
          <this.avatarSellPanel />
        </View>
        {this.getItemButtonWrapper()}
      </ImageBackground >
    )
  }

  inventoryWrapper = () => {
    return (
      <View style={styles.inventoryWrapper}>
        <this.leftWrapper />
        <this.rightWrapper />
      </View>
    )
  }

  render = () => {
    return (
      <SafeAreaView style={styles.container}>
        <this.titleWrapper />
        <this.inventoryWrapper />
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_MAIN,
    padding: 20,
  },
  titleWrapper: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    paddingBottom: 10
  },
  title: {
    fontSize: 25,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.WHITE
  },
  subtitle: {
    fontSize: 20,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.WHITE
  },
  inline: {
    alignItems: 'center',
  },
  leftButtonWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignSelf: 'stretch',
    paddingTop: 8,
  },
  inventoryWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  leftWrapper: {
    flex: 0.3,
    alignSelf: 'stretch',
    flexDirection: 'column',
  },
  itemsList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignSelf: 'stretch',
  },
  itemWrapper: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    borderStyle: 'solid',
    borderColor: COLORS.BG_MAIN,
    borderWidth: 2,
    color: COLORS.WHITE,
    borderRadius: 5,
    width: '30%',
    aspectRatio: 1,
    marginBottom: 4,
  },
  itemImage: {
    resizeMode: 'contain',
    margin: 8
  },
  boxCount: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: COLORS.WHITE,
    color: COLORS.BLACK,
    paddingTop: 1,
    paddingBottom: 1,
    paddingLeft: 4,
    paddingRight: 4,
    fontWeight: '700',
  },
  avatarCountWrapper: {
    alignSelf: 'stretch',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: 5
  },
  avatarCountContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  avatarCountText: {
    fontWeight: '700',
    fontSize: 12,
    color: COLORS.BG_MAIN,
    marginRight: 2
  },
  avatarItemStar: {
    width: 12,
    height: 12
  },
  rightWrapper: {
    flex: 0.7,
    alignSelf: 'stretch',
    resizeMode: 'contain',
  },
  itemTitleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    alignSelf: 'stretch'
  },
  itemTitleText: {
    paddingTop: 4,
    paddingLeft: 8,
    paddingBottom: 4,
    paddingRight: 8,
    fontSize: 20,
    fontWeight: '700',
    backgroundColor: COLORS.WHITE,
    color: COLORS.BG_MAIN,
    borderRadius: 5
  },
  shinyToggleWrapper: {
    alignItems: 'flex-end',
    // flex: 1,
    alignSelf: 'stretch',
  },
  lootPanel: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  sellTextWrapper: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingBottom: 8
  },
  sellText: {
    fontSize: 25,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.WHITE
  },
  sellImage: {
    width: 22,
    height: 22,
    marginLeft: 4
  },
  buttonsWrapper: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingBottom: 10,
  }
});

const mapStateToProps = ({ auth }) => ({
  auth
})

const mapDispatchToProps = dispatch => ({
  updateUser: user => dispatch(updateUser(user)),
  logout: () => dispatch(logout()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Inventory)
