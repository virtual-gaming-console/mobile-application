import React, { Component } from 'react'
import { Image, StyleSheet, Text, View, ScrollView, Button, TouchableWithoutFeedback, ImageBackground } from 'react-native'
import { connect } from "react-redux"
import { updateUser, logout } from '../../actions/auth'
import * as GoogleSignIn from 'expo-google-sign-in'
import COLORS from '../../../colors'
import AuthService from '../../services/AuthService'
import starImage from '../../../assets/star.png'
import HeaderRight from '../../containers/HeaderRight'


class Profile extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'MY PROFILE',
      headerRight: () => <HeaderRight onPressQuestWrapper={() => navigation.navigate('Quest')} />
    }
  }

  state = {
    avatar: null,
  }

  componentDidMount() {
    this.setState({
      avatar: this.props.auth.user.avatar
    })
  }

  isSelectedAvatar = avatar => {
    return this.state.avatar && this.state.avatar.id === avatar.id && this.state.avatar.shiny === avatar.shiny
  }

  handleClickAvatar = avatar => {
    if (!this.isActualAvatar(avatar)) {
      this.setState({
        avatar
      })
    }
  }

  isActualAvatar = avatar => {
    if (!avatar) {
      return false
    }
    return this.props.auth.user.avatar.id === avatar.id && this.props.auth.user.avatar.shiny === avatar.shiny
  }

  getAvatarItemStyle = (avatar) => {
    if (this.isActualAvatar(avatar)) {
      return {
        backgroundColor: 'rgba(0,0,0,.8)',
        opacity: .8
      }
    }

    let style = {}
    let opacity = '.8'
    if (this.isSelectedAvatar(avatar)) {
      style.borderColor = COLORS.WHITE
      opacity = '.9'
    }
    if (avatar.isLegendary) {
      style.backgroundColor = 'rgba(201, 176, 55, ' + opacity + ')'
    } else {
      switch (avatar.rank) {
        case 3:
          style.backgroundColor = 'rgba(215, 215, 215, ' + opacity + ')'
          break
        case 2:
          style.backgroundColor = 'rgba(173, 138, 86, ' + opacity + ')'
          break
        default:
          style.backgroundColor = 'rgba(0,0,0,' + opacity + ')'
          break
      }
    }

    return style
  }

  getSortedAvatarList = () => {
    return this.props.auth.user.unlockedAvatars
      .map(avatarItem => this.isActualAvatar(avatarItem) ? ({ ...avatarItem, index: 0 }) : avatarItem)
      .sort((a, b) => a.index - b.index)
  }

  logout = async () => {
    const { params } = this.props.navigation.state;
    if (params) {
      const { beforeLogout } = params
      if (beforeLogout) {
        beforeLogout()
      }
    }
    if (!__DEV__) {
      await GoogleSignIn.initAsync();
      await GoogleSignIn.signOutAsync();
    }
    await AuthService.logout()
    this.props.logout()
    this.props.navigation.navigate('Home')
  }

  validateProfile = async () => {
    let response = await AuthService.updateProfile({ avatar: this.state.avatar })

    if (response.success) {
      // this.props.updateUser(response.data)
      this.props.navigation.pop()
    }
  }

  render = () => {
    if (!this.props.auth.user) {
      return null
    }

    const fillingList = new Array(8 - this.getSortedAvatarList().length % 8).fill(null)

    return (
      <View style={styles.container}>
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>AVATAR SELECTOR</Text>
          <View style={{ flex: 1 }} />
          <Button title="LOGOUT" onPress={this.logout} color={COLORS.BLACK}></Button>
          <View style={{ width: 10 }} />
          <Button title="INVENTORY" onPress={() => this.props.navigation.push('Inventory')} color={COLORS.BLACK}></Button>
        </View>
        <ScrollView style={{ flex: 1, alignSelf: 'stretch' }}>
          <View style={styles.itemsList}>
            {this.getSortedAvatarList().map((avatar, index) => (
              <TouchableWithoutFeedback onPress={() => this.handleClickAvatar(avatar)} key={index}>
                <ImageBackground
                  style={[styles.itemWrapper, this.getAvatarItemStyle(avatar)]}
                  imageStyle={styles.itemImage}
                  source={{ uri: avatar.image }}>
                  {avatar.shiny && <Image style={styles.avatarItemStar} source={starImage} alt="star_img" />}
                </ImageBackground>
              </TouchableWithoutFeedback>
            ))}
            {fillingList.map((x, index) => <View style={{ width: '12%' }} key={"void" + index} />)}
          </View>
        </ScrollView>
        <View style={[styles.inline, styles.padding, { justifyContent: 'flex-end', alignSelf: 'stretch' }]}>
          <Button title="CANCEL" onPress={() => this.props.navigation.pop()} color={COLORS.BLACK}></Button>
          <View style={{ width: 20 }} />
          <Button title="SAVE" onPress={this.validateProfile} color={COLORS.BLACK} disabled={this.isActualAvatar(this.state.avatar)}></Button>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_MAIN,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    display: 'flex',
    padding: 20,
  },
  titleWrapper: {
    alignSelf: 'stretch',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    display: 'flex',
    paddingBottom: 10
  },
  title: {
    fontSize: 25,
    fontWeight: '700',
    textTransform: 'uppercase',
    color: COLORS.WHITE
  },
  inline: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  padding: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
  },
  itemsList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignSelf: 'stretch',
  },
  itemWrapper: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    borderStyle: 'solid',
    borderColor: COLORS.BG_MAIN,
    borderWidth: 2,
    color: COLORS.WHITE,
    borderRadius: 5,
    width: '12%',
    aspectRatio: 1,
    marginBottom: 4,
  },
  itemImage: {
    resizeMode: 'contain',
    margin: 8
  },
  // avatarListWrapper: {
  //   flex: 1,
  //   display: 'flex',
  //   alignItems: 'flex-start',
  //   flexDirection: 'row',
  //   justifyContent: 'flex-start',
  //   flexWrap: 'wrap',
  //   alignSelf: 'stretch',
  // },
  // avatarItem: {
  //   position: 'relative',
  //   borderStyle: 'solid',
  //   borderColor: COLORS.BG_MAIN,
  //   borderWidth: 2,
  //   color: COLORS.WHITE,
  //   margin: 5,
  //   borderRadius: 5
  // },
  // avatarItemImage: {
  //   width: 75,
  //   height: 75,
  // },
  avatarItemStar: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 12,
    height: 12,
    margin: 2
  },
  text: {
    fontSize: 25,
    color: COLORS.WHITE,
    fontWeight: '700',
    textTransform: "uppercase"
  },
  image: {
    width: 25,
    height: 25,
    marginLeft: 8
  },
  avatarImage: {
    width: 50,
    height: 50,
    margin: 2,
    backgroundColor: COLORS.WHITE,
    borderRadius: 5
  }
});

const mapStateToProps = ({ auth }) => ({
  auth
})

const mapDispatchToProps = dispatch => ({
  updateUser: user => dispatch(updateUser(user)),
  logout: () => dispatch(logout()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile)
