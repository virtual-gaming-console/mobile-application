import React, { Component } from 'react'
import { StyleSheet, TextInput, View, Text, Button } from 'react-native'
import COLORS from '../../../colors'
import LobbyService from "../../services/LobbyService"
import * as Updates from 'expo-updates';
import HeaderRight from '../../containers/HeaderRight'

const UPDATE_STATES = {
    DISMISSED: "UPDATE_STATE_DISMISSED",
    AVAILABLE: "UPDATE_STATE_AVAILABLE",
    DOWNLOADING: "UPDATE_STATE_DOWNLOADING",
    DOWNLOADED: "UPDATE_STATE_DOWNLOADED",
}

class Home extends Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            title: 'HOME',
            headerRight: () => <HeaderRight
                onPressUserWrapper={() => navigation.push('Profile')}
                onUserUnknown={() => navigation.push('UpdateNickname')}
                onPressQuestWrapper={() => navigation.push('Quest')} />
        }
    }

    state = {
        lobbyCode: '',
        lobbyCodeIsValid: false,
        updateState: UPDATE_STATES.DISMISSED,
    }

    componentDidMount() {
        if (!__DEV__) {
            this.checkForUpdates()
        }
    }

    checkForUpdates = async () => {
        let available = (await Updates.checkForUpdateAsync()).isAvailable
        this.setState({
            updateState: available ? UPDATE_STATES.AVAILABLE : UPDATE_STATES.DISMISSED
        })
    }

    onLobbyCodeChangeText = (text) => {
        this.setState({ lobbyCode: text.toUpperCase() }, () => {
            if (this.state.lobbyCode.length === 6) {
                this.tryEnterLobby()
            }
        })
    }

    async tryEnterLobby() {
        const { lobbyCode } = this.state

        const check = await LobbyService.check(lobbyCode)
        if (check.exist) {
            this.setState({
                lobbyCode: '',
            }, () => {
                this.props.navigation.navigate('Controller', { lobbyCode })
            })
        } else {
            this.setState({ lobbyCodeIsValid: false })
        }
    }

    getInputTextColor = () => {
        const { lobbyCode, lobbyCodeIsValid } = this.state

        if (lobbyCode.length < 6) {
            return COLORS.BLACK
        } else {
            if (lobbyCodeIsValid) {
                return COLORS.GREEN
            } else {
                return COLORS.RED
            }
        }
    }

    getValidationText = () => {
        const { lobbyCode, lobbyCodeIsValid } = this.state

        if (lobbyCode.length < 6) {
            return ''
        } else {
            if (lobbyCodeIsValid) {
                return 'Entering lobby...'
            } else {
                return 'Wrong lobby code, you should try to get another one.'
            }
        }
    }

    dismissUpdate = () => {
        this.setState({
            updateState: UPDATE_STATES.DISMISSED
        })
    }

    installUpdate = () => {
        this.setState({
            updateState: UPDATE_STATES.DOWNLOADING
        }, async () => {
            if ((await Updates.fetchUpdateAsync()).isNew) {
                Updates.reloadAsync()
            } else {
                this.dismissUpdate
            }
        })
    }

    updateWrapper = () => {
        if (this.state.updateState === UPDATE_STATES.DISMISSED) {
            return null
        }

        let text = ""
        switch (this.state.updateState) {
            case UPDATE_STATES.AVAILABLE:
                text = "An update is available !"
                break
            case UPDATE_STATES.DOWNLOADING:
                text = "Currently downloading the update ..."
                break
            case UPDATE_STATES.DOWNLOADED:
                text = "Update successfuly downloaded, reloading now ..."
                break
        }

        return (
            <View style={styles.updateWrapper}>
                <Text style={styles.updateText}>{text}</Text>
                <View style={{ flex: 1 }} />
                {this.state.updateState === UPDATE_STATES.AVAILABLE && (
                    <>
                        <Button title="Dismiss" onPress={this.dismissUpdate} />
                        <View style={{ width: 10 }} />
                        <Button title="Install and reload" onPress={this.installUpdate} />
                    </>
                )}
            </View>
        )
    }


    render = () => (
        <View style={styles.container}>
            <Text style={styles.title}>Virtual Gaming Console v1.2.7</Text>
            <View style={styles.lobbyWrapper}>
                <Text style={styles.subtitle}>ENTER LOBBY CODE HERE</Text>
                <TextInput style={[styles.input, { color: this.getInputTextColor() }]}
                    placeholder="ex: XA78BY"
                    placeholderTextColor={COLORS.DARKGREY}
                    onChangeText={this.onLobbyCodeChangeText}
                    value={this.state.lobbyCode}
                    autoCorrect={false}
                    keyboardType="decimal-pad"
                    maxLength={6} />
                <Text style={styles.validation}>{this.getValidationText()}</Text>
            </View>
            <this.updateWrapper />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_MAIN,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        display: 'flex',
        padding: 20
    },
    lobbyWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        display: 'flex'
    },
    title: {
        fontSize: 35,
        fontWeight: '700',
        textTransform: 'uppercase',
        color: COLORS.WHITE,
        padding: 20,
        textAlign: "center"
    },
    subtitle: {
        fontSize: 25,
        fontWeight: '700',
        textTransform: 'uppercase',
        color: COLORS.WHITE,
        padding: 20
    },
    validation: {
        fontSize: 18,
        fontWeight: '700',
        textTransform: 'uppercase',
        color: COLORS.WHITE,
        padding: 15
    },
    input: {
        fontSize: 22,
        fontWeight: '700',
        padding: 20,
        backgroundColor: COLORS.WHITE,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        minWidth: 250
    },
    updateWrapper: {
        width: '70%',
        alignSelf: 'auto',
        backgroundColor: COLORS.WHITE,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    updateText: {
        color: COLORS.BG_MAIN,
        fontSize: 18,
        fontWeight: '700',
        textTransform: 'uppercase',
    }
});

export default Home
