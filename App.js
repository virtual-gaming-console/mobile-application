import React from 'react'
import Home from './src/pages/Home'
import Controller from './src/pages/Controller'
import Profile from './src/pages/Profile'
import Inventory from './src/pages/Inventory'
import UpdateNickname from './src/pages/UpdateNickname'
import Quest from './src/pages/Quest'
import { createAppContainer } from "react-navigation"
import { createStackNavigator } from "react-navigation-stack"
import { Provider } from 'react-redux'
import { store } from './src/helpers/store'

const MainNavigator = createStackNavigator({
  Home,
  Controller,
  Profile,
  Inventory,
  UpdateNickname,
  Quest,
},
  {
    initialRouteName: 'Home',
  })

const App = createAppContainer(MainNavigator)
const AppWithStore = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

export default AppWithStore